package android.os;

import android.os.Parcel;
import android.os.Process;

public class ContainerInfo implements Parcelable {
    /**
     * The name of the container that this object is associated with
     * Default should be the package name
     */
    private String containerName;

    /**
     * The name of the associated process
     */
    private String processName;

    /**
     * The pid of this process; -1 if none
     */
    private int pid;

    /**
     * The uid of this process; -1 if none
     */
    private int uid;

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(containerName);
        dest.writeString(processName);
        dest.writeInt(pid);
        dest.writeInt(uid);
    }

    public void readFromParcel(Parcel source) {
        containerName = source.readString();
        pid = source.readInt();
        uid = source.readInt();
    }

    public static final Creator<ContainerInfo> CREATOR =
        new Creator<ContainerInfo>() {
        public ContainerInfo createFromParcel(Parcel source) {
            return new ContainerInfo(source);
        }
        public ContainerInfo[] newArray(int size) {
            return new ContainerInfo[size];
        }
    };

    public int describeContents() {
        return 0;
    }

    private ContainerInfo(Parcel source) {
        readFromParcel(source);
    }

    /* In case the OS doesn't support multiple processes */
    public ContainerInfo(String containerName, String processName)
    {
        this.pid = 0;
        this.uid = 0;
        this.processName = processName;
        if (containerName == null)
            this.containerName = processName;
        else
            this.containerName = containerName;
    }

    public ContainerInfo(int pid, int uid, String processName, String containerName)
    {
        this(processName, containerName);
        this.pid = pid;
        this.uid = uid;
    }

    public String getContainerName() { return containerName; }
    public void setContainerName(String name) { containerName = name; }
    public String getProcessName() { return processName; }
    public int getPid() { return pid; }
    public int getUid() { return uid; }
}
