package android.os;

import android.os.IInterface;
import android.os.RemoteException;
import android.os.ContainerInfo;

/**
 * Basic interface for finding and publishing system container.
 *
 * An implementation of this interface is usually published as the
 * global context object, which can be retrieved via
 * BinderNative.getContextObject(). An easy way to retrieve this is with
 * the static method BnContainerNameManager.getDefault().
 */
public interface IContainerNameManager extends IInterface
{
    /**
     * Retrieve an existing container with id @a id from the container
     * manager. Blocks for a few seconds for it to be published if it
     * does not already exist.
     */
    public ContainerInfo getContainer(int id) throws RemoteException;

    /**
     * Place a new @a container into the container manager. Returns the
     * generated id for the container
     */
    public int addContainer(ContainerInfo container) throws RemoteException;

    /**
     * Remove an existing container with id @a id from the container
     * manager.
     */
    public boolean removeContainer(int id) throws RemoteException;

    /**
     * Update an existing container with id @a id with a new @a container
     * value. Return the previous @a container value.
     */
    public ContainerInfo updateContainer(int id, ContainerInfo container) throws RemoteException;

    /**
     * Return a list of all currently running container ids.
     */
    public int[] listContainerIds() throws RemoteException;

    /**
     * Return a list of all currently running container names.
     */
    public String[] listContainerNames() throws RemoteException;

    /**
     * Return name associated with given container
     */
    public String getContainerName(int id) throws RemoteException;

    /**
     * Return PID associated with given container
     */
    public int getContainerPid(int id) throws RemoteException;

    /**
     * Return UID associated with given container
     */
    public int getContainerUid(int id) throws RemoteException;

    /**
     * return ID associated with container name
     */
    public int getContainerId(String name) throws RemoteException;

    /**
     * Return IDs associated with pid
     */
    public int[] getContainerIdsFromPid(int pid) throws RemoteException;

    /**
     * Return IDs associated with uid
     */
    public int[] getContainerIdsFromUid(int uid) throws RemoteException;

    static final String descriptor = "android.os.IContainerNameManager";

    final int GET_CONTAINER_TRANSACTION = IBinder.FIRST_CALL_TRANSACTION;
    final int ADD_CONTAINER_TRANSACTION = IBinder.FIRST_CALL_TRANSACTION+1;
    final int DEL_CONTAINER_TRANSACTION = IBinder.FIRST_CALL_TRANSACTION+2;
    final int UPDATE_CONTAINER_TRANSACTION = IBinder.FIRST_CALL_TRANSACTION+3;
    final int LIST_CONTAINER_IDS_TRANSACTION = IBinder.FIRST_CALL_TRANSACTION+4;
    final int LIST_CONTAINER_NAMES_TRANSACTION = IBinder.FIRST_CALL_TRANSACTION+5;
    final int GET_CONTAINER_NAME_TRANSACTION = IBinder.FIRST_CALL_TRANSACTION+6;
    final int GET_CONTAINER_PID_TRANSACTION = IBinder.FIRST_CALL_TRANSACTION+7;
    final int GET_CONTAINER_UID_TRANSACTION = IBinder.FIRST_CALL_TRANSACTION+8;
    final int GET_CONTAINER_ID_TRANSACTION = IBinder.FIRST_CALL_TRANSACTION+9;
    final int GET_CONTAINER_IDS_FROM_PID_TRANSACTION = IBinder.FIRST_CALL_TRANSACTION+10;
    final int GET_CONTAINER_IDS_FROM_UID_TRANSACTION = IBinder.FIRST_CALL_TRANSACTION+11;
}
