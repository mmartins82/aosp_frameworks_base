package android.os;

import android.os.IContainerNameManager;
import android.os.Process;
import android.os.RemoteException;
import android.os.ContainerNameManagerNative;
import android.util.Log;


/**
 * Tools for managing container containers.
 */
public class Container {
    private static final String LOG_TAG = "Container";

    /**
     * Returns elapsed milliseconds of the time this container has run.
     * @return  Returns the number of milliseconds this container has return.
    /**
     * Returns the identifier of this container
     */
    public static final int myCid() {
        int pid = Process.myPid();
        int[] cids = null;

        try {
            IContainerNameManager rm = ContainerNameManagerNative.getDefault();
            cids = rm.getContainerIdsFromPid(pid);
        } catch (RemoteException e) {
            Log.e(LOG_TAG,
                    "Retrieving container IDs through manager failed");
            throw new RuntimeException(
                    "Retrieving container IDs through manager failed", e);
        }

        /* TODO: This is not necessarily correct, but should work for
         * now
         **/
        if (cids == null)
            return -1;

        return cids[0];
    }

    /**
     * Returns a uid for a currently running container.
     * @param cid the container id
     * @return the uid of the container, or -1 if the container is not running.
     * @hide pending API council review
     */
    /* public static final int getUidForCid(int cid) {
        String[] procStatusLabels = { "Cid:" };
        long[] procStatusValues = new long[1];
        procStatusValues[0] = -1;
        Container.readContainerLines("/proc/containers" + cid + "/status", procStatusLabels, procStatusValues);
        return (int) procStatusValues[0];
    }*/

    public static final int getUidForCid(int cid) {
        int uid = -1;

        try {
            IContainerNameManager rm = ContainerNameManagerNative.getDefault();
            uid = rm.getContainerUid(cid);
        } catch (RemoteException e) {
            Log.e(LOG_TAG,
                    "Retrieving container IDs through manager failed");
            throw new RuntimeException(
                    "Retrieving container IDs through manager failed", e);
        }

        return uid;
    }

    /**
     * Returns the parent container id for a currently running container.
     * @param cid the container id
     * @return the parent container id of the container, or -1 if the
     * container is not running or it is not an affiliated container
     * (i.e., it is the only container for a given process.)
     * @hide
     */
    public static final int getParentCid(int cid) {
        String[] procStatusLabels = { "PCid:" };
        long[] procStatusValues = new long[1];
        procStatusValues[0] = -1;
        Container.readContainerLines("/proc/containers/" + cid + "/status", procStatusLabels, procStatusValues);
        return (int) procStatusValues[0];
    }

    /** @hide */
    public static final native void readContainerLines(String path,
            String[] reqFields, long[] outSizes);

    /** @hide */
    //public static final native int[] getCids(String path, int[] lastArray);
    public static final int[] getCids() {
        int[] cids = null;

        try {
            IContainerNameManager rm = ContainerNameManagerNative.getDefault();
            cids = rm.listContainerIds();
        } catch (RemoteException e) {
            Log.e(LOG_TAG,
                    "Retrieving container IDs through manager failed");
            throw new RuntimeException(
                    "Retrieving container IDs through manager failed", e);
        }

        return cids;
    }

    /** @hide */
    public static final int RSC_TERM_MASK = 0xff;
    /** @hide */
    public static final int RSC_ZERO_TERM = 0;
    /** @hide */
    public static final int RSC_SPACE_TERM = (int)' ';
    /** @hide */
    public static final int RSC_TAB_TERM = (int)'\t';
    /** @hide */
    public static final int RSC_COMBINE = 0x100;
    /** @hide */
    public static final int RSC_PARENS = 0x200;
    /** @hide */
    public static final int RSC_OUT_STRING = 0x1000;
    /** @hide */
    public static final int RSC_OUT_LONG = 0x2000;
    /** @hide */
    public static final int RSC_OUT_FLOAT = 0x4000;

    /** @hide */
    public static final native boolean readContainerFile(String file, int[] format,
            String[] outStrings, long[] outLongs, float[] outFloats);

    /** @hide */
    public static final native boolean parseContainerLine(byte[] buffer, int startIndex,
            int endIndex, int[] format, String[] outStrings, long[] outLongs, float[] outFloats);

}
