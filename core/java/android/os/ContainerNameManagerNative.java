package android.os;

import android.os.Binder;
import android.os.IBinder;
import android.os.IContainerNameManager;
import android.os.Parcel;
import android.os.ContainerInfo;

import android.util.Config;
import android.util.Log;
import android.util.Singleton;

/**
 * Native implementation of the resource manager. Most clients will only
 * care about getDefault() and possible asInterface().
 * @hide
 */
public abstract class ContainerNameManagerNative extends Binder implements IContainerNameManager
{
    /**
     * Cast a Binder object into a service manager interface, generating
     * a proxy if needed.
     */
    static public IContainerNameManager asInterface(IBinder obj)
    {
        if (obj == null) {
            return null;
        }
        IContainerNameManager in = 
            (IContainerNameManager)obj.queryLocalInterface(descriptor);
        if (in != null) {
            return in;
        }

        return new ContainerNameManagerProxy(obj);
    }

    /**
     * Retrieve the system's default/global resource manager.
     */
    static public IContainerNameManager getDefault() {
        return gDefault.get();
    }

    /**
     * Retrieve the system's default/global resource manager.
     */
    public ContainerNameManagerNative()
    {
        attachInterface(this, descriptor);
    }

    public boolean onTransact(int code, Parcel data, Parcel reply, int flags) throws RemoteException
    {
        switch (code) {
            case IContainerNameManager.GET_CONTAINER_TRANSACTION: {
                data.enforceInterface(IContainerNameManager.descriptor);
                int id = data.readInt();
                ContainerInfo resource = getContainer(id);
                reply.writeNoException();
                if (resource != null) {
                    reply.writeInt(1);
                    resource.writeToParcel(reply, 0);
                } else {
                    reply.writeInt(0);
                }
                return true;
            }

            case IContainerNameManager.ADD_CONTAINER_TRANSACTION: {
                data.enforceInterface(IContainerNameManager.descriptor);
                ContainerInfo resource = null;
                if (data.readInt() != 0) {
                    resource = ContainerInfo.CREATOR.createFromParcel(data);
                }
                int id = addContainer(resource);
                reply.writeNoException();
                reply.writeInt(id);
                return true;
            }

            case IContainerNameManager.DEL_CONTAINER_TRANSACTION: {
                data.enforceInterface(IContainerNameManager.descriptor);
                int id = data.readInt();
                boolean ret = removeContainer(id);
                reply.writeNoException();
                reply.writeInt(ret ? 1 : 0);
                return true;
            }

            case IContainerNameManager.UPDATE_CONTAINER_TRANSACTION: {
                data.enforceInterface(IContainerNameManager.descriptor);
                int id = data.readInt();
                ContainerInfo resource = null;
                if (data.readInt() != 0) {
                    resource = ContainerInfo.CREATOR.createFromParcel(data);
                }
                ContainerInfo res = updateContainer(id, resource);
                reply.writeNoException();
                if (resource != null) {
                    reply.writeInt(1);
                    resource.writeToParcel(reply, 0);
                } else {
                    reply.writeInt(0);
                }
                return true;
            }

            case IContainerNameManager.LIST_CONTAINER_IDS_TRANSACTION: {
                data.enforceInterface(IContainerNameManager.descriptor);
                int[] list = listContainerIds();
                reply.writeNoException();
                reply.writeIntArray(list);
                return true;
            }

            case IContainerNameManager.LIST_CONTAINER_NAMES_TRANSACTION: {
                data.enforceInterface(IContainerNameManager.descriptor);
                String[] list= listContainerNames();
                reply.writeNoException();
                reply.writeStringArray(list);
                return true;
            }

            case IContainerNameManager.GET_CONTAINER_NAME_TRANSACTION: {
                data.enforceInterface(IContainerNameManager.descriptor);
                int id = data.readInt();
                String name = getContainerName(id);
                reply.writeNoException();
                reply.writeString(name);
                return true;
            }

            case IContainerNameManager.GET_CONTAINER_PID_TRANSACTION: {
                data.enforceInterface(IContainerNameManager.descriptor);
                int id = data.readInt();
                int pid = getContainerPid(id);
                reply.writeNoException();
                reply.writeInt(pid);
                return true;
            }

            case IContainerNameManager.GET_CONTAINER_UID_TRANSACTION: {
                data.enforceInterface(IContainerNameManager.descriptor);
                int id = data.readInt();
                int uid = getContainerUid(id);
                reply.writeNoException();
                reply.writeInt(uid);
                return true;
            }

            case IContainerNameManager.GET_CONTAINER_ID_TRANSACTION: {
                data.enforceInterface(IContainerNameManager.descriptor);
                String name = data.readString();
                int id = getContainerId(name);
                reply.writeNoException();
                reply.writeInt(id);
                return true;
            }

            case IContainerNameManager.GET_CONTAINER_IDS_FROM_PID_TRANSACTION: {
                data.enforceInterface(IContainerNameManager.descriptor);
                int pid = data.readInt();
                int[] ids = getContainerIdsFromPid(pid);
                reply.writeNoException();
                reply.writeIntArray(ids);
                return true;
            }

            case IContainerNameManager.GET_CONTAINER_IDS_FROM_UID_TRANSACTION: {
                data.enforceInterface(IContainerNameManager.descriptor);
                int uid = data.readInt();
                int[] ids = getContainerIdsFromUid(uid);
                reply.writeNoException();
                reply.writeIntArray(ids);
                return true;
            }
        }

        return super.onTransact(code, data, reply, flags);
    }

    public IBinder asBinder()
    {
        return this;
    }

    private static final Singleton<IContainerNameManager> gDefault = new Singleton<IContainerNameManager>() {
        protected IContainerNameManager create() {
            IBinder b = ServiceManager.getService("containername");
            if (Config.LOGV) {
                Log.v("ContainerNameManager", "default service binder = " + b);
            }
            IContainerNameManager cm = asInterface(b);
            if (Config.LOGV) {
                Log.v("ContainerNameManager", "default service = " + b);
            }
            return cm;
        }
    };

}

class ContainerNameManagerProxy implements IContainerNameManager {
    public ContainerNameManagerProxy(IBinder remote) {
        mRemote = remote;
    }

    public IBinder asBinder() {
        return mRemote;
    }

    public ContainerInfo getContainer(int id) throws RemoteException {
        Parcel data = Parcel.obtain();
        Parcel reply = Parcel.obtain();
        data.writeInterfaceToken(IContainerNameManager.descriptor);
        data.writeInt(id);
        mRemote.transact(GET_CONTAINER_TRANSACTION, data, reply, 0);
        reply.readException();
        int res = reply.readInt();
        ContainerInfo resource = null;
        if (res != 0) {
            resource = ContainerInfo.CREATOR.createFromParcel(reply);
        }
        reply.recycle();
        data.recycle();
        return resource;
    }

    public int addContainer(ContainerInfo resource) throws RemoteException {
        Parcel data = Parcel.obtain();
        Parcel reply = Parcel.obtain();
        data.writeInterfaceToken(IContainerNameManager.descriptor);
        if (resource != null) {
            data.writeInt(1);
            resource.writeToParcel(data, 0);
        } else {
            data.writeInt(0);
        }
        mRemote.transact(ADD_CONTAINER_TRANSACTION, data, reply, 0);
        reply.readException();
        int res = reply.readInt();
        data.recycle();
        reply.recycle();
        return res;
    }

    public boolean removeContainer(int id) throws RemoteException {
        Parcel data = Parcel.obtain();
        Parcel reply = Parcel.obtain();
        data.writeInterfaceToken(IContainerNameManager.descriptor);
        data.writeInt(id);
        mRemote.transact(DEL_CONTAINER_TRANSACTION, data, reply, 0);
        reply.readException();
        boolean ret = (reply.readInt() != 0);
        reply.recycle();
        data.recycle();

        return ret;
    }

    public ContainerInfo updateContainer(int id, ContainerInfo resource) throws RemoteException {
        Parcel data = Parcel.obtain();
        Parcel reply = Parcel.obtain();
        data.writeInterfaceToken(IContainerNameManager.descriptor);
        data.writeInt(id);
        if (resource != null) {
            data.writeInt(1);
            resource.writeToParcel(data, 0);
        } else {
            data.writeInt(0);
        }
        mRemote.transact(UPDATE_CONTAINER_TRANSACTION, data, reply, 0);
        reply.readException();
        ContainerInfo res = null;
        int haveContainer = reply.readInt();
        if (haveContainer != 0)
            res = ContainerInfo.CREATOR.createFromParcel(reply);
        data.recycle();
        reply.recycle();
        return res;
    }

    public int[] listContainerIds() throws RemoteException {
        int[] list = null;
        Parcel data = Parcel.obtain();
        Parcel reply = Parcel.obtain();
        data.writeInterfaceToken(IContainerNameManager.descriptor);
        mRemote.transact(LIST_CONTAINER_IDS_TRANSACTION, data, reply, 0);
        reply.readException();
        reply.readIntArray(list);
        reply.recycle();
        data.recycle();
        return list;
    }

    public String[] listContainerNames() throws RemoteException {
        Parcel data = Parcel.obtain();
        Parcel reply = Parcel.obtain();
        data.writeInterfaceToken(IContainerNameManager.descriptor);
        mRemote.transact(LIST_CONTAINER_NAMES_TRANSACTION, data, reply, 0);
        reply.readException();
        String[] list = reply.readStringArray();
        reply.recycle();
        data.recycle();
        return list;
    }

    public String getContainerName(int id) throws RemoteException {
        Parcel data = Parcel.obtain();
        Parcel reply = Parcel.obtain();
        data.writeInterfaceToken(IContainerNameManager.descriptor);
        data.writeInt(id);
        mRemote.transact(GET_CONTAINER_NAME_TRANSACTION, data, reply, 0);
        reply.readException();
        String name = reply.readString();
        reply.recycle();
        data.recycle();
        return name;
    }

    public int getContainerPid(int id) throws RemoteException {
        Parcel data = Parcel.obtain();
        Parcel reply = Parcel.obtain();
        data.writeInterfaceToken(IContainerNameManager.descriptor);
        data.writeInt(id);
        mRemote.transact(GET_CONTAINER_PID_TRANSACTION, data, reply, 0);
        reply.readException();
        int pid = reply.readInt();
        reply.recycle();
        data.recycle();
        return pid;
    }

    public int getContainerUid(int id) throws RemoteException {
        Parcel data = Parcel.obtain();
        Parcel reply = Parcel.obtain();
        data.writeInterfaceToken(IContainerNameManager.descriptor);
        data.writeInt(id);
        mRemote.transact(GET_CONTAINER_UID_TRANSACTION, data, reply, 0);
        reply.readException();
        int uid = reply.readInt();
        reply.recycle();
        data.recycle();
        return uid;
    }

    public int getContainerId(String name) throws RemoteException {
        Parcel data = Parcel.obtain();
        Parcel reply = Parcel.obtain();
        data.writeInterfaceToken(IContainerNameManager.descriptor);
        data.writeString(name);
        mRemote.transact(GET_CONTAINER_ID_TRANSACTION, data, reply, 0);
        reply.readException();
        int id = reply.readInt();
        reply.recycle();
        data.recycle();
        return id;
    }

    public int[] getContainerIdsFromPid(int pid) throws RemoteException {
        int[] ids = null;
        Parcel data = Parcel.obtain();
        Parcel reply = Parcel.obtain();
        data.writeInterfaceToken(IContainerNameManager.descriptor);
        data.writeInt(pid);
        mRemote.transact(GET_CONTAINER_IDS_FROM_PID_TRANSACTION, data, reply, 0);
        reply.readException();
        reply.readIntArray(ids);
        reply.recycle();
        data.recycle();
        return ids;
    }

    public int[] getContainerIdsFromUid(int uid) throws RemoteException {
        int[] ids = null;
        Parcel data = Parcel.obtain();
        Parcel reply = Parcel.obtain();
        data.writeInterfaceToken(IContainerNameManager.descriptor);
        data.writeInt(uid);
        mRemote.transact(GET_CONTAINER_IDS_FROM_UID_TRANSACTION, data, reply, 0);
        reply.readException();
        reply.readIntArray(ids);
        reply.recycle();
        data.recycle();
        return ids;
    }

    private IBinder mRemote;
}
