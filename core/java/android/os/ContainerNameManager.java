package android.os;

import android.content.Context;
import android.os.Handler;
import android.os.IBinder;
import android.os.IContainerNameManager;
import android.os.Process;
import android.os.ContainerInfo;
import android.os.ContainerNameManagerNative;
import android.os.ServiceManager;
import android.util.Log;

/**
 * Keep track of all processes/threads and other container entities
 */
public class ContainerNameManager {
    public static final String SERVICE_NAME = "containername";
    private static final String TAG = "ContainerNameManager";
    private static boolean DEBUG = false;
    private static boolean localLOGV = DEBUG || android.util.Config.LOGV;

    private final Context mContext;
    private final Handler mHandler;

    private static final String FILE_PREFIX = "containers-";

    private static final int OWN_PROCESS_ID = Process.myPid();

    static IContainerNameManager sService = null;

    /**
     * @hide
     */
    ContainerNameManager(Context context, Handler handler) {
        mContext = context;
        mHandler = handler;
    }

    /**
     * @param context the context that the ContainerNameService runs in
     */
    public static ContainerNameManager getService(Context context) {
        return (ContainerNameManager) context.getSystemService(Context.CONTAINER_NAME_SERVICE);
    }

    public ContainerInfo getContainer(int id) {
        /*mContext.enforceCallingOrSelfPermission(
                android.Manifest.permission.VIEW_CONTAINERS, null);*/

        ContainerInfo r = null;

        if (DEBUG)    Log.v(
            TAG, "getContainerInfo " + id
            + ": " + id);

        try {
            r = ContainerNameManagerNative.getDefault().getContainer(id);
        } catch (RemoteException e) {
        }

        return r;
    }


    public int addContainer(ContainerInfo container) {
        int id = -1;

        /*mContext.enforceCallingOrSelfPermission(
                android.Manifest.permission.REGISTER_CONTAINERS, null);*/

        if (DEBUG) Log.v(TAG, "registering container " + container.getContainerName());

        try {
            id = ContainerNameManagerNative.getDefault().addContainer(container);
        } catch (RemoteException e) {
        }

        return id;
    }

    public boolean removeContainer(int id) {
        boolean ret = false;

        /*mContext.enforceCallingOrSelfPermission(
                android.Manifest.permission.REGISTER_CONTAINERS, null);*/

        if (DEBUG) Log.v(TAG, "removing container with id " + id);

        try {
            ret = ContainerNameManagerNative.getDefault().removeContainer(id);
        } catch (RemoteException e) {
        }

        return ret;
    }

    public ContainerInfo updateContainer(int id, ContainerInfo container) {
        ContainerInfo res = null;

        /*mContext.enforceCallingOrSelfPermission(
                android.Manifest.permission.REGISTER_CONTAINERS, null);*/

        if (DEBUG) Log.v(TAG, "updating container " + container.getContainerName() + " with id : " + id);

        try {
            res = ContainerNameManagerNative.getDefault().updateContainer(id, container);
        } catch (RemoteException e) {
        }

        return res;
    }

    public int[] listContainerIds() {
        int[] ids = null;

        /*mContext.enforceCallingOrSelfPermission(
                android.Manifest.permission.VIEW_CONTAINERS, null);*/

        if (DEBUG) Log.v(TAG, "listing container ids");

        try {
            ids = ContainerNameManagerNative.getDefault().listContainerIds();
        } catch (RemoteException e) {
        }

        return ids;
    }

    public String[] listContainerNames() {
        String[] names = null;

        /*mContext.enforceCallingOrSelfPermission(
                android.Manifest.permission.VIEW_CONTAINERS, null);*/

        if (DEBUG) Log.v(TAG, "listing container names");

        try {
            names = ContainerNameManagerNative.getDefault().listContainerNames();
        } catch (RemoteException e) {
        }

        return names;
    }

    public String getContainerName(int id) {
        String name = null;

        if (DEBUG) Log.v(TAG, "Retrieving name from container " + id);

        try {
            name = ContainerNameManagerNative.getDefault().getContainerName(id);
        } catch  (RemoteException e) {
        }

        return name;
    }

    public int getContainerPid(int id) {
        int pid = -1;

        if (DEBUG) Log.v(TAG, "Retrieving pid from container " + id);

        try {
            id = ContainerNameManagerNative.getDefault().getContainerPid(id);
        } catch (RemoteException e) {
        }

        return pid;
    }

    public int getContainerUid(int id) {
        int uid = -1;

        if (DEBUG) Log.v(TAG, "Retrieving uid from container " + id);

        try {
            uid = ContainerNameManagerNative.getDefault().getContainerUid(id);
        } catch (RemoteException e) {
        }

        return uid;
    }

    public int getContainerId(String name) {
        int id = -1;

        /*mContext.enforceCallingOrSelfPermission(
                android.Manifest.permission.VIEW_CONTAINERS, null);*/

        if (DEBUG) Log.v(TAG, "Searching for id of container " + name);

        try {
            id = ContainerNameManagerNative.getDefault().getContainerId(name);
        } catch (RemoteException e) {
        }

        return id;
    }

    public int[] getContainerIdsFromPid(int pid) {
        int[] ids = null;

        if (DEBUG) Log.v(TAG, "Retrieving container ids for pid " + pid);

        try {
            ids = ContainerNameManagerNative.getDefault().getContainerIdsFromPid(pid);
        } catch (RemoteException e) {
        }

        return ids;
    }

    public int[] getContainerIdsFromUid(int uid) {
        int[] ids = null;

        if (DEBUG) Log.v(TAG, "Retrieving container ids for uid " + uid);

        try {
            ids = ContainerNameManagerNative.getDefault().getContainerIdsFromUid(uid);
        } catch (RemoteException e) {
        }

        return ids;
    }

    public void enforceCallingPermission() {
        if (Binder.getCallingPid() == Process.myPid()) {
            return;
        }
        /*mContext.enforcePermission(android.Manifest.permission.REGISTER_CONTAINERS,
                Binder.getCallingPid(), Binder.getCallingUid(), null);*/
    }

}
