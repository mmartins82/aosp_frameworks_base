/*
  Copyright (C) 2011 The University of Michigan

  This program is free software: { you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.    See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.    If not, see <http: {//www.gnu.org/licenses/>.

  Please send inquiries to powertutor@umich.edu
*/

package edu.umich.PowerTutor;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public abstract interface PowerNotifications extends IInterface {
    static final String DESCRIPTOR = "edu.umich.PowerTutor.PowerNotifications";

    static final int TRANSACTION_noteStartWakelock = 1;
    static final int TRANSACTION_noteStopWakelock = 2;
    static final int TRANSACTION_noteStartSensor = 3;
    static final int TRANSACTION_noteStopSensor = 4;
    static final int TRANSACTION_noteStartGps = 5;
    static final int TRANSACTION_noteStopGps = 6;
    static final int TRANSACTION_noteScreenBrightness = 7;
    static final int TRANSACTION_noteStartMedia = 8;
    static final int TRANSACTION_noteStopMedia = 9;
    static final int TRANSACTION_noteVideoSize = 10;
    static final int TRANSACTION_noteSystemMediaCall = 11;
    static final int TRANSACTION_noteScreenOn = 12;
    static final int TRANSACTION_noteScreenOff = 13;
    static final int TRANSACTION_noteInputEvent = 14;
    static final int TRANSACTION_noteUserActivity = 15;
    static final int TRANSACTION_notePhoneOn = 16;
    static final int TRANSACTION_notePhoneOff = 17;
    static final int TRANSACTION_notePhoneDataConnectionState = 18;
    static final int TRANSACTION_noteWifiOn = 19;
    static final int TRANSACTION_noteWifiOff = 20;
    static final int TRANSACTION_noteWifiRunning = 21;
    static final int TRANSACTION_noteWifiStopped = 22;
    static final int TRANSACTION_noteBluetoothOn = 23;
    static final int TRANSACTION_noteBluetoothOff = 24;
    static final int TRANSACTION_noteFullWifiLockAcquired = 25;
    static final int TRANSACTION_noteFullWifiLockReleased = 26;
    static final int TRANSACTION_noteScanWifiLockAcquired = 27;
    static final int TRANSACTION_noteScanWifiLockReleased = 28;
    static final int TRANSACTION_noteWifiMulticastEnabled = 29;
    static final int TRANSACTION_noteWifiMulticastDisabled = 30;
    static final int TRANSACTION_setOnBattery = 31;
    static final int TRANSACTION_recordCurrentLevel = 32;
    static final int TRANSACTION_noteVideoOn = 33;
    static final int TRANSACTION_noteVideoOff = 34;
    static final int TRANSACTION_noteAudioOn = 35;
    static final int TRANSACTION_noteAudioOff = 36;
    static final int TRANSACTION_getDescriptor = 37;

    // These are the notifications that are actually supported. The rest have
    // potential to be supported but aren't needed at the moment so aren't
    // actually hooked.
    void noteStartWakelock(int uid, String name, int type) throws RemoteException;

    void noteStopWakelock(int uid, String name, int type) throws RemoteException;

    void noteStartSensor(int uid, int sensor) throws RemoteException;

    void noteStopSensor(int uid, int sensor) throws RemoteException;

    void noteStartGps(int uid) throws RemoteException;

    void noteStopGps(int uid) throws RemoteException;

    void noteScreenBrightness(int brightness) throws RemoteException;

    void noteStartMedia(int uid, int id) throws RemoteException;

    void noteStopMedia(int uid, int id) throws RemoteException;

    void noteVideoSize(int uid, int id, int width, int height) throws RemoteException;

    void noteSystemMediaCall(int uid) throws RemoteException;

    void noteScreenOn() throws RemoteException;

    void noteScreenOff() throws RemoteException;

    void noteInputEvent() throws RemoteException;

    void noteUserActivity(int uid, int event) throws RemoteException;

    void notePhoneOn() throws RemoteException;

    void notePhoneOff() throws RemoteException;

    void notePhoneDataConnectionState(int dataType, boolean hasData) throws RemoteException;

    void noteWifiOn(int uid) throws RemoteException;

    void noteWifiOff(int uid) throws RemoteException;

    void noteWifiRunning() throws RemoteException;

    void noteWifiStopped() throws RemoteException;

    void noteBluetoothOn() throws RemoteException;

    void noteBluetoothOff() throws RemoteException;

    void noteFullWifiLockAcquired(int uid) throws RemoteException;

    void noteFullWifiLockReleased(int uid) throws RemoteException;

    void noteScanWifiLockAcquired(int uid) throws RemoteException;

    void noteScanWifiLockReleased(int uid) throws RemoteException;

    void noteWifiMulticastEnabled(int uid) throws RemoteException;

    void noteWifiMulticastDisabled(int uid) throws RemoteException;

    void setOnBattery(boolean onBattery, int level) throws RemoteException;

    void recordCurrentLevel(int level) throws RemoteException;

    /*
     * Also got rid of the non-notification calls. byte[] getStatistics(); long
     * getAwakeTimeBattery(); long getAwakeTimePlugged();
     */

    /* Added functions to the normal IBatteryStats interface. */
    void noteVideoOn(int uid) throws RemoteException;

    void noteVideoOff(int uid) throws RemoteException;

    void noteAudioOn(int uid) throws RemoteException;

    void noteAudioOff(int uid) throws RemoteException;


    public static abstract class Stub extends Binder implements PowerNotifications {

        public Stub()
        {
            attachInterface(this, PowerNotifications.DESCRIPTOR);
        }

        public static PowerNotifications asInterface(IBinder obj)
        {
            if (obj == null) {
                return null;
            }

            PowerNotifications pm = (PowerNotifications) obj.queryLocalInterface(PowerNotifications.DESCRIPTOR);
            if (pm != null)
                return pm;

            return new PowerNotificationsProxy(obj);
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int code, Parcel data, Parcel reply, int flags) throws RemoteException
        {
            switch (code)
            {
                case TRANSACTION_noteStartWakelock: {
                    data.enforceInterface(PowerNotifications.DESCRIPTOR);
                    noteStartWakelock(data.readInt(), data.readString(), data.readInt());
                    reply.writeNoException();
                    return true;
                }

                case TRANSACTION_noteStopWakelock: {
                    data.enforceInterface(PowerNotifications.DESCRIPTOR);
                    noteStopWakelock(data.readInt(), data.readString(), data.readInt());
                    reply.writeNoException();
                    return true;
                }

                case TRANSACTION_noteStartSensor: {
                    data.enforceInterface(PowerNotifications.DESCRIPTOR);
                    noteStartSensor(data.readInt(), data.readInt());
                    reply.writeNoException();
                    return true;
                }

                case TRANSACTION_noteStopSensor: {
                    data.enforceInterface(PowerNotifications.DESCRIPTOR);
                    noteStopSensor(data.readInt(), data.readInt());
                    reply.writeNoException();
                    return true;
                }

                case TRANSACTION_noteStartGps: {
                    data.enforceInterface(PowerNotifications.DESCRIPTOR);
                    noteStartGps(data.readInt());
                    reply.writeNoException();
                    return true;
                }

                case TRANSACTION_noteStopGps: {
                    data.enforceInterface(PowerNotifications.DESCRIPTOR);
                    noteStopGps(data.readInt());
                    reply.writeNoException();
                    return true;
                }

                case TRANSACTION_noteScreenBrightness: {
                    data.enforceInterface(PowerNotifications.DESCRIPTOR);
                    noteScreenBrightness(data.readInt());
                    reply.writeNoException();
                    return true;
                }

                case TRANSACTION_noteStartMedia: {
                    data.enforceInterface(PowerNotifications.DESCRIPTOR);
                    noteStartMedia(data.readInt(), data.readInt());
                    reply.writeNoException();
                    return true;
                }

                case TRANSACTION_noteStopMedia: {
                    data.enforceInterface(PowerNotifications.DESCRIPTOR);
                    noteStopMedia(data.readInt(), data.readInt());
                    reply.writeNoException();
                    return true;
                }

                case TRANSACTION_noteVideoSize: {
                    data.enforceInterface(PowerNotifications.DESCRIPTOR);
                    noteVideoSize(data.readInt(), data.readInt(), data.readInt(), data.readInt());
                    reply.writeNoException();
                    return true;
                }

                case TRANSACTION_noteSystemMediaCall: {
                    data.enforceInterface(PowerNotifications.DESCRIPTOR);
                    noteSystemMediaCall(data.readInt());
                    reply.writeNoException();
                    return true;
                }

                case TRANSACTION_noteScreenOn: {
                    data.enforceInterface(PowerNotifications.DESCRIPTOR);
                    noteScreenOn();
                    reply.writeNoException();
                    return true;
                }

                case TRANSACTION_noteScreenOff: {
                    data.enforceInterface(PowerNotifications.DESCRIPTOR);
                    noteScreenOff();
                    reply.writeNoException();
                    return true;
                }

                case TRANSACTION_noteInputEvent: {
                    data.enforceInterface(PowerNotifications.DESCRIPTOR);
                    noteInputEvent();
                    reply.writeNoException();
                    return true;
                }

                case TRANSACTION_noteUserActivity: {
                    data.enforceInterface(PowerNotifications.DESCRIPTOR);
                    noteUserActivity(data.readInt(), data.readInt());
                    reply.writeNoException();
                    return true;
                }

                case TRANSACTION_notePhoneOn: {
                    data.enforceInterface(PowerNotifications.DESCRIPTOR);
                    notePhoneOn();
                    reply.writeNoException();
                    return true;
                }

                case TRANSACTION_notePhoneOff: {
                    data.enforceInterface(PowerNotifications.DESCRIPTOR);
                    notePhoneOff();
                    reply.writeNoException();
                    return true;
                }

                case TRANSACTION_notePhoneDataConnectionState: {
                    boolean hasData;
                    data.enforceInterface(PowerNotifications.DESCRIPTOR);
                    int dataType = data.readInt();

                    if (data.readInt() == 0)
                        hasData = false;
                    else
                        hasData = true;

                    notePhoneDataConnectionState(dataType, hasData);
                    reply.writeNoException();
                    return true;
                }

                case TRANSACTION_noteWifiOn: {
                    data.enforceInterface(PowerNotifications.DESCRIPTOR);
                    noteWifiOn(data.readInt());
                    reply.writeNoException();
                    return true;
                }

                case TRANSACTION_noteWifiOff: {
                    data.enforceInterface(PowerNotifications.DESCRIPTOR);
                    noteWifiOff(data.readInt());
                    reply.writeNoException();
                    return true;
                }

                case TRANSACTION_noteWifiRunning: {
                    data.enforceInterface(PowerNotifications.DESCRIPTOR);
                    noteWifiRunning();
                    reply.writeNoException();
                    return true;
                }

                case TRANSACTION_noteWifiStopped: {
                    data.enforceInterface(PowerNotifications.DESCRIPTOR);
                    noteWifiStopped();
                    reply.writeNoException();
                    return true;
                }

                case TRANSACTION_noteBluetoothOn: {
                    data.enforceInterface(PowerNotifications.DESCRIPTOR);
                    noteBluetoothOn();
                    reply.writeNoException();
                    return true;
                }

                case TRANSACTION_noteBluetoothOff: {
                    data.enforceInterface(PowerNotifications.DESCRIPTOR);
                    noteBluetoothOff();
                    reply.writeNoException();
                    return true;
                }

                case TRANSACTION_noteFullWifiLockAcquired: {
                    data.enforceInterface(PowerNotifications.DESCRIPTOR);
                    noteFullWifiLockAcquired(data.readInt());
                    reply.writeNoException();
                    return true;
                }

                case TRANSACTION_noteFullWifiLockReleased: {
                    data.enforceInterface(PowerNotifications.DESCRIPTOR);
                    noteFullWifiLockReleased(data.readInt());
                    reply.writeNoException();
                    return true;
                }

                case TRANSACTION_noteScanWifiLockAcquired: {
                    data.enforceInterface(PowerNotifications.DESCRIPTOR);
                    noteScanWifiLockAcquired(data.readInt());
                    reply.writeNoException();
                    return true;
                }

                case TRANSACTION_noteScanWifiLockReleased: {
                    data.enforceInterface(PowerNotifications.DESCRIPTOR);
                    noteScanWifiLockReleased(data.readInt());
                    reply.writeNoException();
                    return true;
                }

                case TRANSACTION_noteWifiMulticastEnabled: {
                    data.enforceInterface(PowerNotifications.DESCRIPTOR);
                    noteWifiMulticastEnabled(data.readInt());
                    reply.writeNoException();
                    return true;
                }

                case TRANSACTION_noteWifiMulticastDisabled: {
                    data.enforceInterface(PowerNotifications.DESCRIPTOR);
                    noteWifiMulticastDisabled(data.readInt());
                    reply.writeNoException();
                    return true;
                }

                case TRANSACTION_setOnBattery: {
                    boolean onBattery;
                    data.enforceInterface(PowerNotifications.DESCRIPTOR);
                    if (data.readInt() == 0)
                        onBattery = false;
                    else
                        onBattery =  true;
                    setOnBattery(onBattery, data.readInt());
                    reply.writeNoException();
                    return true;
                }

                case TRANSACTION_recordCurrentLevel: {
                    data.enforceInterface(PowerNotifications.DESCRIPTOR);
                    recordCurrentLevel(data.readInt());
                    reply.writeNoException();
                    return true;
                }

                case TRANSACTION_noteVideoOn: {
                    data.enforceInterface(PowerNotifications.DESCRIPTOR);
                    noteVideoOn(data.readInt());
                    reply.writeNoException();
                    return true;
                }

                case TRANSACTION_noteVideoOff: {
                    data.enforceInterface(PowerNotifications.DESCRIPTOR);
                    noteVideoOff(data.readInt());
                    reply.writeNoException();
                    return true;
                }

                case TRANSACTION_noteAudioOn: {
                    data.enforceInterface(PowerNotifications.DESCRIPTOR);
                    noteAudioOn(data.readInt());
                    reply.writeNoException();
                    return true;
                }

                case TRANSACTION_noteAudioOff: {
                    data.enforceInterface(PowerNotifications.DESCRIPTOR);
                    noteAudioOff(data.readInt());
                    reply.writeNoException();
                    return true;
                }

                case TRANSACTION_getDescriptor: {
                    reply.writeString(PowerNotifications.DESCRIPTOR);
                    return true;
                }
            }

            return super.onTransact(code, data, reply, flags);
        }
    }
}

class PowerNotificationsProxy implements PowerNotifications
{
    private IBinder mRemote;

    PowerNotificationsProxy(IBinder remote)
    {
        mRemote = remote;
    }

    public IBinder asBinder()
    {
        return mRemote;
    }

    public String getInterfaceDescriptor()
    {
        return PowerNotifications.DESCRIPTOR;
    }

    public void noteStartWakelock(int uid, String name, int type) throws RemoteException
    {
        Parcel data = Parcel.obtain();
        Parcel reply = Parcel.obtain();

        data.writeInterfaceToken(PowerNotifications.DESCRIPTOR);
        data.writeInt(uid);
        data.writeString(name);
        data.writeInt(type);
        mRemote.transact(PowerNotifications.TRANSACTION_noteStartWakelock, data, reply, 0);
        reply.readException();

        reply.recycle();
        data.recycle();
    }

    public void noteStopWakelock(int uid, String name, int type) throws RemoteException
    {
        Parcel data = Parcel.obtain();
        Parcel reply = Parcel.obtain();

        data.writeInterfaceToken(PowerNotifications.DESCRIPTOR);
        data.writeInt(uid);
        data.writeString(name);
        data.writeInt(type);
        mRemote.transact(PowerNotifications.TRANSACTION_noteStopWakelock, data, reply, 0);
        reply.readException();

        reply.recycle();
        data.recycle();
    }

    public void noteStartSensor(int uid, int sensor) throws RemoteException
    {
        Parcel data = Parcel.obtain();
        Parcel reply = Parcel.obtain();

        data.writeInterfaceToken(PowerNotifications.DESCRIPTOR);
        data.writeInt(uid);
        data.writeInt(sensor);
        mRemote.transact(PowerNotifications.TRANSACTION_noteStartSensor, data, reply, 0);
        reply.readException();

        reply.recycle();
        data.recycle();
    }

    public void noteStopSensor(int uid, int sensor) throws RemoteException
    {
        Parcel data = Parcel.obtain();
        Parcel reply = Parcel.obtain();

        data.writeInterfaceToken(PowerNotifications.DESCRIPTOR);
        data.writeInt(uid);
        data.writeInt(sensor);
        mRemote.transact(PowerNotifications.TRANSACTION_noteStopSensor, data, reply, 0);
        reply.readException();

        reply.recycle();
        data.recycle();
    }

    public void noteStartGps(int uid) throws RemoteException
    {
        Parcel data = Parcel.obtain();
        Parcel reply = Parcel.obtain();

        data.writeInterfaceToken(PowerNotifications.DESCRIPTOR);
        data.writeInt(uid);
        mRemote.transact(PowerNotifications.TRANSACTION_noteStartGps, data, reply, 0);
        reply.readException();

        reply.recycle();
        data.recycle();
    }

    public void noteStopGps(int uid) throws RemoteException
    {
        Parcel data = Parcel.obtain();
        Parcel reply = Parcel.obtain();

        data.writeInterfaceToken(PowerNotifications.DESCRIPTOR);
        data.writeInt(uid);
        mRemote.transact(PowerNotifications.TRANSACTION_noteStopGps, data, reply, 0);
        reply.readException();

        reply.recycle();
        data.recycle();
    }

    public void noteScreenBrightness(int uid) throws RemoteException
    {
        Parcel data = Parcel.obtain();
        Parcel reply = Parcel.obtain();

        data.writeInterfaceToken(PowerNotifications.DESCRIPTOR);
        data.writeInt(uid);
        mRemote.transact(PowerNotifications.TRANSACTION_noteScreenBrightness, data, reply, 0);
        reply.readException();

        reply.recycle();
        data.recycle();
    }

    public void noteStartMedia(int uid, int id) throws RemoteException
    {
        Parcel data = Parcel.obtain();
        Parcel reply = Parcel.obtain();

        data.writeInterfaceToken(PowerNotifications.DESCRIPTOR);
        data.writeInt(uid);
        data.writeInt(id);
        mRemote.transact(PowerNotifications.TRANSACTION_noteStartMedia, data, reply, 0);
        reply.readException();

        reply.recycle();
        data.recycle();
    }

    public void noteStopMedia(int uid, int id) throws RemoteException
    {
        Parcel data = Parcel.obtain();
        Parcel reply = Parcel.obtain();

        data.writeInterfaceToken(PowerNotifications.DESCRIPTOR);
        data.writeInt(uid);
        data.writeInt(id);
        mRemote.transact(PowerNotifications.TRANSACTION_noteStopMedia, data, reply, 0);
        reply.readException();

        reply.recycle();
        data.recycle();
    }

    public void noteVideoSize(int uid, int id, int width, int height) throws RemoteException
    {
        Parcel data = Parcel.obtain();
        Parcel reply = Parcel.obtain();

        data.writeInterfaceToken(PowerNotifications.DESCRIPTOR);
        data.writeInt(uid);
        data.writeInt(id);
        data.writeInt(width);
        data.writeInt(height);
        mRemote.transact(PowerNotifications.TRANSACTION_noteVideoSize, data, reply, 0);
        reply.readException();

        reply.recycle();
        data.recycle();
    }

    public void noteSystemMediaCall(int uid) throws RemoteException
    {
        Parcel data = Parcel.obtain();
        Parcel reply = Parcel.obtain();

        data.writeInterfaceToken(PowerNotifications.DESCRIPTOR);
        data.writeInt(uid);
        mRemote.transact(PowerNotifications.TRANSACTION_noteSystemMediaCall, data, reply, 0);
        reply.readException();

        reply.recycle();
        data.recycle();
    }

    public void noteScreenOn() throws RemoteException
    {
        Parcel data = Parcel.obtain();
        Parcel reply = Parcel.obtain();

        data.writeInterfaceToken(PowerNotifications.DESCRIPTOR);
        mRemote.transact(PowerNotifications.TRANSACTION_noteScreenOn, data, reply, 0);
        reply.readException();

        reply.recycle();
        data.recycle();
    }

    public void noteScreenOff() throws RemoteException
    {
        Parcel data = Parcel.obtain();
        Parcel reply = Parcel.obtain();

        data.writeInterfaceToken(PowerNotifications.DESCRIPTOR);
        mRemote.transact(PowerNotifications.TRANSACTION_noteScreenOff, data, reply, 0);
        reply.readException();

        reply.recycle();
        data.recycle();
    }

    public void noteInputEvent() throws RemoteException
    {
        Parcel data = Parcel.obtain();
        Parcel reply = Parcel.obtain();

        data.writeInterfaceToken(PowerNotifications.DESCRIPTOR);
        mRemote.transact(PowerNotifications.TRANSACTION_noteInputEvent, data, reply, 0);
        reply.readException();

        reply.recycle();
        data.recycle();
    }

    public void noteUserActivity(int uid, int event) throws RemoteException
    {
        Parcel data = Parcel.obtain();
        Parcel reply = Parcel.obtain();

        data.writeInterfaceToken(PowerNotifications.DESCRIPTOR);
        data.writeInt(uid);
        data.writeInt(event);
        mRemote.transact(PowerNotifications.TRANSACTION_noteUserActivity, data, reply, 0);
        reply.readException();

        reply.recycle();
        data.recycle();
    }

    public void notePhoneOn() throws RemoteException
    {
        Parcel data = Parcel.obtain();
        Parcel reply = Parcel.obtain();

        data.writeInterfaceToken(PowerNotifications.DESCRIPTOR);
        mRemote.transact(PowerNotifications.TRANSACTION_notePhoneOn, data, reply, 0);
        reply.readException();

        reply.recycle();
        data.recycle();
    }

    public void notePhoneOff() throws RemoteException
    {
        Parcel data = Parcel.obtain();
        Parcel reply = Parcel.obtain();

        data.writeInterfaceToken(PowerNotifications.DESCRIPTOR);
        mRemote.transact(PowerNotifications.TRANSACTION_notePhoneOff, data, reply, 0);
        reply.readException();

        reply.recycle();
        data.recycle();
    }

    public void notePhoneDataConnectionState(int dataType, boolean hasData) throws RemoteException
    {
        Parcel data = Parcel.obtain();
        Parcel reply = Parcel.obtain();

        data.writeInterfaceToken(PowerNotifications.DESCRIPTOR);
        data.writeInt(dataType);

        if (hasData)
        {
            data.writeInt(1);
            mRemote.transact(PowerNotifications.TRANSACTION_notePhoneDataConnectionState, data, reply, 0);
            reply.readException();
        }

        reply.recycle();
        data.recycle();
    }

    public void noteWifiOn(int uid) throws RemoteException
    {
        Parcel data = Parcel.obtain();
        Parcel reply = Parcel.obtain();

        data.writeInterfaceToken(PowerNotifications.DESCRIPTOR);
        data.writeInt(uid);
        mRemote.transact(PowerNotifications.TRANSACTION_noteWifiOn, data, reply, 0);
        reply.readException();

        reply.recycle();
        data.recycle();
    }

    public void noteWifiOff(int uid) throws RemoteException
    {
        Parcel data = Parcel.obtain();
        Parcel reply = Parcel.obtain();

        data.writeInterfaceToken(PowerNotifications.DESCRIPTOR);
        data.writeInt(uid);
        mRemote.transact(PowerNotifications.TRANSACTION_noteWifiOff, data, reply, 0);
        reply.readException();

        reply.recycle();
        data.recycle();
    }

    public void noteWifiRunning() throws RemoteException
    {
        Parcel data = Parcel.obtain();
        Parcel reply = Parcel.obtain();

        data.writeInterfaceToken(PowerNotifications.DESCRIPTOR);
        mRemote.transact(PowerNotifications.TRANSACTION_noteWifiRunning, data, reply, 0);
        reply.readException();

        reply.recycle();
        data.recycle();
    }

    public void noteWifiStopped() throws RemoteException
    {
        Parcel data = Parcel.obtain();
        Parcel reply = Parcel.obtain();

        data.writeInterfaceToken(PowerNotifications.DESCRIPTOR);
        mRemote.transact(PowerNotifications.TRANSACTION_noteWifiStopped, data, reply, 0);
        reply.readException();

        reply.recycle();
        data.recycle();
    }

    public void noteBluetoothOn() throws RemoteException
    {
        Parcel reply = Parcel.obtain();
        Parcel data = Parcel.obtain();

        data.writeInterfaceToken(PowerNotifications.DESCRIPTOR);
        mRemote.transact(PowerNotifications.TRANSACTION_noteBluetoothOn, data, reply, 0);
        reply.readException();

        reply.recycle();
        data.recycle();
    }

    public void noteBluetoothOff() throws RemoteException
    {
        Parcel data = Parcel.obtain();
        Parcel reply = Parcel.obtain();

        data.writeInterfaceToken(PowerNotifications.DESCRIPTOR);
        mRemote.transact(PowerNotifications.TRANSACTION_noteBluetoothOff, data, reply, 0);
        reply.readException();

        reply.recycle();
        data.recycle();
    }

    public void noteFullWifiLockAcquired(int uid) throws RemoteException
    {
        Parcel data = Parcel.obtain();
        Parcel reply = Parcel.obtain();

        data.writeInterfaceToken(PowerNotifications.DESCRIPTOR);
        data.writeInt(uid);
        mRemote.transact(PowerNotifications.TRANSACTION_noteFullWifiLockAcquired, data, reply, 0);
        reply.readException();

        reply.recycle();
        data.recycle();
    }

    public void noteFullWifiLockReleased(int uid) throws RemoteException
    {
        Parcel data = Parcel.obtain();
        Parcel reply = Parcel.obtain();

        data.writeInterfaceToken(PowerNotifications.DESCRIPTOR);
        data.writeInt(uid);
        mRemote.transact(PowerNotifications.TRANSACTION_noteFullWifiLockReleased, data, reply, 0);
        reply.readException();

        reply.recycle();
        data.recycle();
    }

    public void noteScanWifiLockAcquired(int uid) throws RemoteException
    {
        Parcel data = Parcel.obtain();
        Parcel reply = Parcel.obtain();

        data.writeInterfaceToken(PowerNotifications.DESCRIPTOR);
        data.writeInt(uid);
        mRemote.transact(PowerNotifications.TRANSACTION_noteScanWifiLockAcquired, data, reply, 0);
        reply.readException();

        reply.recycle();
        data.recycle();
    }

    public void noteScanWifiLockReleased(int uid) throws RemoteException
    {
        Parcel data = Parcel.obtain();
        Parcel reply = Parcel.obtain();

        data.writeInterfaceToken(PowerNotifications.DESCRIPTOR);
        data.writeInt(uid);
        mRemote.transact(PowerNotifications.TRANSACTION_noteScanWifiLockReleased, data, reply, 0);
        reply.readException();

        reply.recycle();
        data.recycle();
    }

    public void noteWifiMulticastEnabled(int uid) throws RemoteException
    {
        Parcel data = Parcel.obtain();
        Parcel reply = Parcel.obtain();

        data.writeInterfaceToken(PowerNotifications.DESCRIPTOR);
        data.writeInt(uid);
        mRemote.transact(PowerNotifications.TRANSACTION_noteWifiMulticastEnabled, data, reply, 0);
        reply.readException();

        reply.recycle();
        data.recycle();
    }

    public void noteWifiMulticastDisabled(int uid) throws RemoteException
    {
        Parcel data = Parcel.obtain();
        Parcel reply = Parcel.obtain();

        data.writeInterfaceToken(PowerNotifications.DESCRIPTOR);
        data.writeInt(uid);
        mRemote.transact(PowerNotifications.TRANSACTION_noteWifiMulticastDisabled, data, reply, 0);
        reply.readException();

        reply.recycle();
        data.recycle();
    }

    public void setOnBattery(boolean onBattery, int level) throws RemoteException
    {
        Parcel data = Parcel.obtain();
        Parcel reply = Parcel.obtain();

        data.writeInterfaceToken(PowerNotifications.DESCRIPTOR);
        if (onBattery) {
            data.writeInt(1);
            data.writeInt(level);
            mRemote.transact(PowerNotifications.TRANSACTION_setOnBattery, data, reply, 0);
            reply.readException();
        }

        reply.recycle();
        data.recycle();
    }

    public void recordCurrentLevel(int level) throws RemoteException
    {
        Parcel data = Parcel.obtain();
        Parcel reply = Parcel.obtain();

        data.writeInterfaceToken(PowerNotifications.DESCRIPTOR);
        data.writeInt(level);
        mRemote.transact(PowerNotifications.TRANSACTION_recordCurrentLevel, data, reply, 0);
        reply.readException();

        reply.recycle();
        data.recycle();
    }

    public void noteVideoOn(int uid) throws RemoteException
    {
        Parcel data = Parcel.obtain();
        Parcel reply = Parcel.obtain();

        data.writeInterfaceToken(PowerNotifications.DESCRIPTOR);
        data.writeInt(uid);
        mRemote.transact(PowerNotifications.TRANSACTION_noteVideoOn, data, reply, 0);
        reply.readException();

        reply.recycle();
        data.recycle();
    }

    public void noteVideoOff(int uid) throws RemoteException
    {
        Parcel data = Parcel.obtain();
        Parcel reply = Parcel.obtain();

        data.writeInterfaceToken(PowerNotifications.DESCRIPTOR);
        data.writeInt(uid);
        mRemote.transact(PowerNotifications.TRANSACTION_noteVideoOff, data, reply, 0);
        reply.readException();

        reply.recycle();
        data.recycle();
    }

    public void noteAudioOn(int uid) throws RemoteException
    {
        Parcel data = Parcel.obtain();
        Parcel reply = Parcel.obtain();

        data.writeInterfaceToken(PowerNotifications.DESCRIPTOR);
        data.writeInt(uid);
        mRemote.transact(PowerNotifications.TRANSACTION_noteAudioOn, data, reply, 0);
        reply.readException();

        reply.recycle();
        data.recycle();
    }

    public void noteAudioOff(int uid) throws RemoteException
    {
        Parcel data = Parcel.obtain();
        Parcel reply = Parcel.obtain();

        data.writeInterfaceToken(PowerNotifications.DESCRIPTOR);
        data.writeInt(uid);
        mRemote.transact(PowerNotifications.TRANSACTION_noteAudioOff, data, reply, 0);
        reply.readException();

        reply.recycle();
        data.recycle();
    }
}
