aosp_frameworks_base
====================

Description
--------------------

A fine-grained resource-container manager for Android Ice Cream
Sandwich.

How to Build
--------------------

1. Download and setup the Android SDK from
   https://developer.android.com/sdk

2. Download the Android Open Source Project (AOSP) source tree.
   Instructions can be found at
   http://source.android.com/source/downloading.html

3. Replace the AOSP's frameworks/base repository with the one provided
   here.

4. Fetch the Android Linux. The goldfish project contains the kernel for
   the emulated platforms.

<pre>
   $ git clone https://android.googlesource.com/kernel/goldfish.git
   $ cd goldfish
   $ git branch -a
   $ git checkout -t origin/android-goldfish-3.4 -b goldfish
</pre>

5. To support Quire-like call-stack tracking support, apply the
   following patches before compiling the ROM image/SDK.

   a. From the root of kernel source-code dir (must be Android compatible):

      0001-Kernel-Added-call-stack-tracking.patch

   b. From the root of AOSP source tree:

      0001-Binder-Added-call-stack-to-transaction-data.patch

6. Build the kernel like your normally do on Linux

<pre>
   $ export PATH=<ANDROID BASEDIR>/prebuilt/linux-x86/toolchain/arm-eabi-4.4.3/bin:$PATH
   $ export ARCH=arm
   $ export SUBARCH=arm
   $ export CROSS_COMPILE=arm-eabi-

   $ make goldfish_armv7_defconfig
   $ make
</pre>

7. From the root  of AOSP source tree compile Android using the lunch
   script.

8. To run the emulator with the Goldfish kernel you just compiled, issue
   the following command:

<pre>
   $ path/to/emulator -kernel path/to/goldfish/arch/arm/boot/zImage
</pre>

NOTE: The current version has only been compiled and tested with the
android-4.0.4_r2.1 branch.
