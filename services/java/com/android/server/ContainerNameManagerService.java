package com.android.server;

import com.android.server.Watchdog;

import android.content.Context;
import android.os.Debug;
import android.os.Parcel;
import android.os.Process;
import android.os.RemoteException;
import android.os.ContainerInfo;
import android.os.ContainerNameManager;
import android.os.ContainerNameManagerNative;
import android.os.ServiceManager;
import android.util.Slog;

import java.util.HashMap;
import java.util.Set;
import java.util.HashSet;
import java.util.Map;

public final class ContainerNameManagerService extends ContainerNameManagerNative
    implements Watchdog.Monitor {
    static final String TAG = "ContainerNameManager";
    static final boolean DEBUG = false;
    static final boolean localLOGV = false;

    static final int MY_PID = Process.myPid();

    private static ContainerNameManagerService instance = null;

    /* map <id, container_info> */
    private final Map<Integer, ContainerInfo> mContainers = 
            new HashMap<Integer, ContainerInfo>();

    /* The following are used as auxiliary indices */
    /* inverted index: map<res_name, id> */
    private final Map<String, Integer> mIds = new HashMap<String, Integer>();

    /* inverted index: map<pid, id> */
    private final Map<Integer, Set<Integer> > mPids = new HashMap<Integer, Set<Integer> >();

    /* inverted index: map<uid, id> */
    private final Map<Integer, Set<Integer> > mUids = new HashMap<Integer, Set<Integer> >();

    private final Object mLock = new Object();

    private ContainerNameManagerService() {
        if (DEBUG) Slog.v(TAG, "ContainerNameService startup");
        // Add outself to the Watchdog monitors.
        Watchdog.getInstance().addMonitor(this);
    }

    public static void setSystemProcess() {
        if (instance == null) {
            instance = new ContainerNameManagerService();
            ContainerInfo res = new ContainerInfo(MY_PID, Process.getUidForPid(MY_PID), "com.android.server.ContainerNameManagerService", TAG);
            instance.addContainer(res);

            ServiceManager.addService(Context.CONTAINER_NAME_SERVICE, instance);
        }
    }

    public final ContainerInfo getContainer(int id) {
        ContainerInfo res;

        synchronized (mLock) {
            res = mContainers.get(id);
        }

        return res;
    }

    public final int addContainer(ContainerInfo container) {
        Set<Integer> idsForPid;
        Set<Integer> idsForUid;

        // Invalid container
        if (container == null)
            return -1;

        int id = ContainerIdFactory.getInstance().newContainerId();
        synchronized (mLock) {
            mContainers.put(id, container);
            mIds.put(container.getContainerName(), id);

            if (mPids.containsKey(container.getPid()))
                idsForPid = mPids.get(container.getPid());
            else {
                idsForPid = new HashSet();
                mPids.put(container.getPid(), idsForPid);
            }

            idsForPid.add(id);

            if (mUids.containsKey(container.getUid()))
                idsForUid = mUids.get(container.getUid());
            else {
                idsForUid = new HashSet();
                mUids.put(container.getUid(), idsForUid);
            }

            idsForUid.add(id);
        }

        return id;
    }

    public final boolean removeContainer(int id) {
        boolean ret = false;

        synchronized (mLock) {
            ContainerInfo res = mContainers.remove(id);

            if (res != null) {
                ret = (mIds.remove(res.getContainerName()) != null) &&
                       removeFromPid(id) && removeFromUid(id);
            }
        }

        return ret;
    }

    public final ContainerInfo updateContainer(int id, ContainerInfo container) {
        ContainerInfo res;
        Set<Integer> ids;

        synchronized (mLock) {
            res = mContainers.put(id, container);

            /* Only update secondary indexes if old and new containers
             * are different
             */
            if (res != container) {

                if (res.getContainerName() != container.getContainerName()) {
                    mIds.remove(res.getContainerName());
                    mIds.put(container.getContainerName(), id);
                }

                if (res.getPid() != container.getPid()) {
                    ids = mPids.get(res.getPid());
                    ids.remove(id);

                    ids = mPids.get(new Integer(container.getPid()));
                    ids.add(id);
                }

                if (res.getUid() != container.getUid()) {
                    ids = mUids.get(res.getUid());
                    ids.remove(id);

                    ids = mPids.get(container.getUid());
                    ids.add(id);
                }
            }
        }

        return res;
    }

    public final int[] listContainerIds() {
        Integer[] intList;
        int[] list;

        synchronized (mLock) {
            intList = (Integer[]) mContainers.keySet().toArray();
        }

        list = new int[intList.length];
        for (int i = 0; i < intList.length; i++) {
            list[i] = intList[i].intValue();
        }

        return list;
    }

    public final String[] listContainerNames() {
        String[] list;

        synchronized (mLock) {
            list = (String[]) mIds.keySet().toArray();
        }

        return list;
    }

    public final String getContainerName(int id) {
        ContainerInfo res = getContainer(id);

        if (res == null)
            return null;

        return res.getContainerName();
    }

    public final int getContainerPid(int id) {
        ContainerInfo res = getContainer(id);

        if (res == null)
            return -1;

        return res.getPid();
    }

    public final int getContainerUid(int id) {
        ContainerInfo res = getContainer(id);

        if (res == null)
            return -1;

        return res.getUid();
    }

    public final int getContainerId(String name) {
        Integer id;

        synchronized (mLock) {
            id = (Integer) mIds.get(name);
        }

        if (id == null)
            return -1;

        return id.intValue();
    }

    public final int[] getContainerIdsFromPid(int pid) {
        Integer[] intList;
        int[] ids;

        synchronized (mLock) {
            intList = (Integer[]) mPids.values().toArray();
        }

        ids = new int[intList.length];
        for (int i = 0; i < intList.length; i++) {
            ids[i] = intList[i].intValue();
        }

        return ids;
    }

    public final int[] getContainerIdsFromUid(int uid) {
        Integer[] intList;
        int[] ids;

        synchronized (mLock) {
            intList = (Integer[]) mUids.values().toArray();
        }

        ids = new int[intList.length];
        for (int i = 0; i < intList.length; i++) {
            ids[i] = intList[i].intValue();
        }

        return ids;
    }

    /* In this method we try to acquire our lock to make sure that we
     * have not deadlocked */
    public void monitor() {
        synchronized (mLock) { }
    }

    private boolean removeFromPid(int id) {
        boolean ret = false;

        for (Set<Integer> ids : mPids.values()) {
            if (ids.contains(id)) {
                ids.remove(id);
                ret = true;
            }
        }

        return ret;
    }

    private boolean removeFromUid(int id) {
        boolean ret = false;

        for (Set<Integer> ids : mUids.values()) {
            if (ids.contains(id)) {
                ids.remove(id);
                ret = true;
            }
        }

        return ret;
    }
}

/**
 * A <code>ContainerIdFactory</code> generates a container ID (similar, but
 * more generic than a pid)
 */
class ContainerIdFactory {

    /**
     * initial seed
     */
    private int containerId = 1;

    /**
     * @link
     * @shapeType PatternLink
     * @pattern Singleton
     * @supplierRole Singleton factory
     */
    /*# private ContainerIdFactory _containerIdFactory; */
    private static ContainerIdFactory instance = null;

    protected ContainerIdFactory() {
    }

    public int newContainerId() {
        containerId += 1;

        return containerId;
    }

    public boolean isValidContainerId(String id) {
        int _id;

        try {
            _id = Integer.parseInt(id);
        } catch(NumberFormatException nfe) {
            return false;
        }

        return (_id > 0 ? true : false);
    }

    public static void main(String[] args) {
        ContainerIdFactory vif = ContainerIdFactory.getInstance();

        int id = vif.newContainerId();
        System.out.println("new container id : " + id);
    }

    public static ContainerIdFactory getInstance() {
        if (instance == null) {
            synchronized(ContainerIdFactory.class) {
                if (instance == null) {
                    instance = new ContainerIdFactory();
                }
            }
        }
        return instance;
    }
}

